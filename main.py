from ui.add_task import InteractiveInterface
from base.base import Base

if __name__ == '__main__':
    Base()
    run = InteractiveInterface()
    run.ui_show()

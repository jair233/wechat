import copy
import datetime
from os import system
from time import sleep

import wx
import csv

from constants import PROJECT_PATH, MODEL_FILE_MSG, MODEL_FILE_PATH, sX, screen_scale_rate, APP_NAME
from task.task import QuitGroupChat, FindGroup
from logger.logger import Logger

log = Logger()


class InteractiveInterface:
    def __init__(self):
        self.app = wx.App()
        x = 350 * screen_scale_rate
        y = 140 * screen_scale_rate
        self.window = wx.Frame(None, title=APP_NAME, style=wx.DEFAULT_FRAME_STYLE | wx.STAY_ON_TOP | wx.CAPTION,
                               pos=(0, 0), size=(int(x), int(y)))
        self.panel = wx.Panel(self.window)
        self.icon = wx.Icon(f'{PROJECT_PATH}/ico.ico', wx.BITMAP_TYPE_ICO)
        self.window.SetIcon(self.icon)
        self.lbl_file_path = wx.StaticText(self.panel, label="文件路径:")
        self.file_path = wx.TextCtrl(self.panel, style=wx.TE_READONLY)
        self.get_file_path = wx.Button(self.panel, -1, label="选择文件")
        self.model_file_btn = wx.Button(self.panel, -1, label="查看模板")
        self.start_button = wx.Button(self.panel, -1, label="开始")
        self.check_group_info = wx.Button(self.panel, -1, label="采集当前微信的群信息")

        x = 300 * screen_scale_rate
        y = 150 * screen_scale_rate
        self.window_execute = wx.Frame(self.window, title="执行记录", size=(int(x), int(y)),
                                       pos=(int(sX / 2), 0), style=wx.CAPTION | wx.STAY_ON_TOP)
        self.panel_execute = wx.Panel(self.window_execute)
        self.execute_log = wx.StaticText(self.panel_execute, -1, style=wx.ALIGN_LEFT | wx.ST_ELLIPSIZE_MIDDLE)
        self.execute_log_count = 1

    def ui_layout(self):

        top = wx.BoxSizer()
        top.Add(self.lbl_file_path, proportion=1, flag=wx.ALL, border=15)
        top.Add(self.file_path, proportion=3, flag=wx.TOP | wx.RIGHT | wx.BOTTOM, border=10)
        bottom = wx.BoxSizer()  # 输入框
        bottom.Add(self.get_file_path, flag=wx.TOP | wx.LEFT, border=15)
        bottom.Add(self.model_file_btn, flag=wx.TOP | wx.LEFT, border=15)
        bottom.Add(self.start_button, flag=wx.TOP | wx.LEFT, border=15)

        page = wx.BoxSizer(wx.VERTICAL)
        page.Add(top)
        page.Add(bottom)
        page.Add(self.check_group_info, flag=wx.TOP | wx.LEFT, border=15)

        self.window.Center()
        self.panel.SetSizer(page)

    def bind_button(self):
        self.get_file_path.Bind(wx.EVT_BUTTON, self.open_file)
        self.model_file_btn.Bind(wx.EVT_BUTTON, self.open_model_file)
        self.start_button.Bind(wx.EVT_BUTTON, self.start_task)
        self.check_group_info.Bind(wx.EVT_BUTTON, self.check_group)

    def check_group(self, event):
        self.check_group_info.SetLabelText('3秒后开始')
        sleep(1)
        self.check_group_info.SetLabelText('2秒后开始')
        sleep(1)
        self.check_group_info.SetLabelText('1秒后开始')
        sleep(1)
        self.window.Show(False)
        self.window_execute.Show(True)
        self.execute_log.SetLabelText(f'正在采集群信息')
        obj = FindGroup()
        runner = obj.run()
        if runner == "WeChat not found!":
            self.check_group_info.SetLabelText('采集当前微信的群信息')
            self.window.Show(True)
            self.window_execute.Show(False)
            self.error_msg('微信页面没有打开, 请打开微信!')
            return
        now = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file = f"{PROJECT_PATH}/execute_data/{now}.csv"
        if runner:
            self.window.Show(True)
            self.window_execute.Show(False)
            obj.group_list.insert(0, ["群名称"])
            write_file(file, obj.group_list)
            self.check_group_info.SetLabelText('采集当前微信的群信息')
            if self.select_execute('群信息已采集完成, 是否查看'):
                system(file)
        else:
            self.check_group_info.SetLabelText('采集当前微信的群信息')
            self.error_msg('采集群信息过程中出错, 请重新采集')
            self.window.Show(True)
            self.window_execute.Show(False)

    def open_file(self, event):
        dlg_open = wx.FileDialog(self.panel, message="打开文件", wildcard="*.csv", style=wx.FD_OPEN)
        if dlg_open.ShowModal() == wx.ID_OK:
            self.file_path.Clear()
            self.file_path.AppendText(dlg_open.GetPath())

    def open_model_file(self, event):
        try:
            res = read_file(f'{MODEL_FILE_PATH}/model.csv')
            log.info(MODEL_FILE_PATH)
            a = copy.deepcopy(MODEL_FILE_MSG)
            a.pop(0)
            if res == a:
                log.info('打开文件1')
                system(f'"{MODEL_FILE_PATH}/model.csv"')
                return
            elif res == '2':
                pass
            else:
                if self.select_execute('模板文件数据被修改,是否覆盖数据?'):
                    pass
                else:
                    return
        except:
            pass
        try:
            write_file()
        except Exception as e:
            log.error(e)
            self.error_msg('模板文件已经打开')
            return
        log.info('打开文件2')
        system(f'"{MODEL_FILE_PATH}/model.csv"')

    def start_task(self, event):
        self.window_execute.Show(True)
        now = datetime.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        file_path = self.file_path.GetValue()
        if file_path.replace('\\', '/') == fr'{MODEL_FILE_PATH}/model.csv':
            self.window_execute.Show(False)
            self.error_msg('执行文件与模板文件不能是同一个文件, 请另存为一个新文件!')
            self.file_path.Clear()
            return
        if not file_path:
            self.window_execute.Show(False)
            self.error_msg('请选择文件!')
            return
        data = read_file(file_path)
        if data == "1":
            self.window_execute.Show(False)
            self.error_msg('请根据模板文件填写正确信息!')
            return
        elif data == "2":
            self.window_execute.Show(False)
            self.error_msg('读取文件失败, 请保存正在编辑的文件并关闭编辑器!')
            return
        self.execute_log.SetLabel('连接微信中...')
        self.start_button.SetLabelText('3秒后开始')
        sleep(1)
        self.start_button.SetLabelText('2秒后开始')
        sleep(1)
        self.start_button.SetLabelText('1秒后开始')
        sleep(1)
        self.window.Show(False)
        sleep(5)
        res = [["群名称", "结果"]]
        obj = QuitGroupChat()
        self.execute_log.SetForegroundColour((0, 0, 0))
        for i in data:
            try:
                if len(i) > 1:
                    if i[1] != "成功":
                        self.execute_log.SetLabelText(f'开始删除: {i[0]}')
                        runner = obj.quit_group(i[0])
                        if runner == "WeChat not found!":
                            self.window.Show(True)
                            self.window_execute.Show(False)
                            self.error_msg('微信页面没有打开, 请打开微信!')
                            self.start_button.SetLabelText('开始')
                            return
                        elif runner:
                            self.execute_log.SetForegroundColour((0, 225, 0))
                            self.execute_log.SetLabelText(f'{i[0]}: 删除成功!')
                            res.append([i[0], f"成功"])
                            sleep(1)
                            self.execute_log.SetForegroundColour((0, 0, 0))

                    else:
                        res.append([i[0], i[1]])
                else:
                    self.execute_log.SetLabelText(f'开始删除: {i[0]}')
                    runner = obj.quit_group(i[0])
                    if runner == "WeChat not found!":
                        self.window.Show(True)
                        self.window_execute.Show(False)
                        self.error_msg('微信页面没有打开, 请打开微信!')
                        self.start_button.SetLabelText('开始')
                        return
                    elif runner:
                        self.execute_log.SetForegroundColour((0, 225, 0))
                        self.execute_log.SetLabelText(f'{i[0]}: 删除成功!')
                        res.append([i[0], f"成功"])
                        sleep(1)
                        self.execute_log.SetForegroundColour((0, 0, 0))
            except:
                self.execute_log.SetForegroundColour((225, 0, 0))
                self.execute_log.SetLabelText(f'{i[0]}: 删除失败!')
                res.append([i[0], "失败"])
                sleep(1)
                self.execute_log.SetForegroundColour((0, 0, 0))
        res_file = f"{PROJECT_PATH}/execute_data/执行结果{now}.csv"

        self.start_button.SetLabelText('任务已完成')
        sleep(1)
        self.start_button.SetLabelText('开始')
        if len(res) > 1:
            self.window_execute.Show(False)
            self.window.Show(True)
            if self.select_execute('是否生成结果并查看?'):
                write_file(res_file, res)
                sleep(1)
                system(f'"{res_file}"')
        else:
            self.execute_log.SetForegroundColour((225, 0, 0))
            for i in range(5):
                self.execute_log.SetLabelText(f'文件中没有数据!')
                sleep(0.3)
                self.execute_log.SetLabelText(f'')
                sleep(0.3)
            sleep(1)
            self.window_execute.Show(False)
            self.window.Show(True)

    def error_msg(self, msg: str, title="提示"):
        dlg = wx.MessageDialog(None, msg, title, wx.YES_DEFAULT | wx.ICON_QUESTION)
        if dlg.ShowModal() == wx.ID_YES:
            dlg.Destroy()

    def select_execute(self, msg: str, title="提示"):
        dlg = wx.MessageDialog(None, msg, title, wx.YES_NO | wx.ICON_QUESTION)
        if dlg.ShowModal() == wx.ID_YES:
            dlg.Destroy()
            return True
        dlg.Destroy()

    def ui_show(self):
        self.ui_layout()
        self.bind_button()
        self.window.Show(True)
        self.app.MainLoop()


def write_file(file_path=None, rows: list = None):
    log.info(MODEL_FILE_PATH)
    with open(file=file_path if file_path else f'{MODEL_FILE_PATH}/model.csv', mode='w', newline='',
              encoding='utf_8_sig', errors='ignore') as fp:
        file = csv.writer(fp)
        file.writerows(rows if rows else MODEL_FILE_MSG)


def read_file(file_path):
    try:
        res = []
        with open(file=file_path if file_path else f'{MODEL_FILE_PATH}/model.csv', mode='r', encoding='utf_8_sig') as fp:
            file = csv.reader(fp)
            count = 0
            for i in file:
                log.info(i[0])
                if count == 0:
                    count += 1
                    if i[0] != '群名称':
                        return "1"
                    else:
                        continue
                res.append(i)
        return res
    except:
        return "2"


if __name__ == '__main__':
    obj = InteractiveInterface()
    obj.ui_show()
    # model_file()
    # read_file()

from .base import Base, sleep, mouse
from constants import screen_scale_rate


class QuitGroupChat(Base):

    def init_chat_page(self):
        if self.win.child_window(title='聊天', control_type='Button').exists(timeout=0.2):
            self.log.info('进入聊天页')
            self.click_one(title='聊天', control_type='Button')
            return True
        else:
            return

    # user 为需要找到的好友名称，如果信息列表中存在，则直接点击，不存在则搜索
    def __find_group(self, name=''):
        if name == '':
            raise ValueError('群名称不可为空!')

        if self.win.child_window(title=name, control_type='Text').exists(timeout=0.2):
            self.click_one(title=name, control_type='Text')
            return True
        else:
            try:
                # self.click_one(title='搜索', control_type='Edit', times=2)
                # 这里必须要等待一定时间，等待搜索框加载出来，时间可以自行尝试着缩短

                self.send_keys("^f")
                sleep(0.2)
                self.log.info(f"输入'{name}'")
                count = 0
                if ' ' in name:
                    name1 = name.split(' ')
                    self.log.info(name1)
                    for i in name1:
                        self.send_keys(i)
                        if count != len(name1)-1:
                            self.send_keys('{SPACE}')
                        count += 1
                else:
                    self.send_keys(name)
                sleep(0.2)
                res = self.win.child_window(title=name, control_type='ListItem').exists(timeout=0.2)
                if res:
                    self.click_one(title=name, control_type='ListItem')
                    return True
                else:
                    self.log.info(f'没有进入"{name}"聊天页面')
                    return
            except Exception as e:
                self.log.error(f"搜索'{name}'过程中出错{e}")
                return

    def __click_delete_and_quit(self, group_name):
        if self.win.child_window(title=group_name, control_type='Button').exists(timeout=0.2):
            group_name1 = self.win.child_window(title=group_name, control_type='Button').window_text()
            if group_name1 != group_name:
                return
            self.log.info('点击进入群操作页面')
            self.click_one(title=group_name, control_type='Button')
            sleep(0.2)
        if self.win.child_window(title='搜索群成员', control_type='Edit').exists(timeout=0.2):
            self.log.info('已经打开聊天信息页')
            self.send_keys('{END}')
            sleep(0.2)
        if self.win.child_window(title='删除并退出', control_type='Button').exists(timeout=0.2):
            self.log.info('点击删除并退出')
            self.click_one(title='删除并退出', control_type='Button')
            sleep(0.2)
        if self.win.child_window(title='确定', control_type='Button').exists(timeout=0.2):
            self.log.info('点击确定')
            self.click_one(title='确定', control_type='Button')
            sleep(0.2)
            return True
        raise "群聊不存在"

    def quit_group(self, group_name=None):
        open_wechat = self.init_chat_page()
        if open_wechat:
            if self.__find_group(group_name if group_name else '0620安全测试'):
                if self.__click_delete_and_quit(group_name if group_name else '0620安全测试'):
                    return True
        else:
            return "WeChat not found!"


class FindGroup(QuitGroupChat):
    def __init__(self):
        super().__init__()
        self.group_list = []
        self.first_item = ''
        self.bottom = 20

    def go_top(self):
        chat_list = self.win.child_window(title='会话', control_type='List')
        box_button = self.win.child_window(title='折叠的群聊', control_type='Button')
        if box_button.exists(timeout=0.2):
            self.click_one(title='折叠的群聊', control_type='Button')
        if chat_list.exists(timeout=0.2):
            self.click_one(title='会话', control_type='List', coord_type=3)
            self.send_keys('{HOME}')

    def get_page_info(self):
        """
        :param bottom: 列表右下角相对位移
        :return:
        """
        chat_list = self.win.child_window(title='会话', control_type='List')
        count = 0
        for i in chat_list:
            if i.window_text() == self.first_item:
                self.log.info('当前页第一项与上一次一样,采集完毕, 退出')
                return True
            if count == 0:
                self.first_item = i.window_text()
                self.click_one(title=f"{i.window_text()}", control_type="ListItem", coord_type=8)
            elif count == len(list(chat_list))-1:
                self.click_one(title=f"{i.window_text()}", control_type="ListItem", coord_type=2)
            else:
                self.click_one(title=f"{i.window_text()}", control_type="ListItem")
            box_list = self.win.child_window(title='折叠的群聊', control_type='List')
            if box_list.exists(timeout=0.2):
                self.in_box()
            else:
                if self.is_group():
                    if [i.window_text()] in self.group_list:
                        pass
                    else:
                        self.log.info(f'找到一个群聊"{i.window_text()}"')
                        self.group_list.append([i.window_text()])
            count += 1
        self.log.info('当前页面已经采集完毕, 准备翻到下一页')
        position = self.win.child_window(title="会话", control_type="List", found_index=0).rectangle()
        mouse.click(button="left", coords=(int(position.right-5*screen_scale_rate),
                                           int(position.bottom-self.bottom*screen_scale_rate)))

    def is_group(self):
        voice_button = self.win.child_window(title='语音聊天', control_type='Button')
        video_button = self.win.child_window(title="视频聊天", control_type='Button')
        webcast_button = self.win.child_window(title="直播", control_type='Button')
        res = []
        if voice_button.exists(timeout=0.2):
            self.log.info('找到语音聊天元素')
            res.append(True)
        if video_button.exists(timeout=0.2):
            self.log.info('找到视频聊天元素')
            res.append(False)
        if webcast_button.exists(timeout=0.2):
            self.log.info('找到直播元素')
            res.append(True)
        if res:
            if not all(res):
                self.log.info('当前目标不是一个群')
            return all(res)

    def in_box(self):
        """在折叠中的群聊"""
        first_item = ''
        self.log.info('开始添加折叠中的群聊')
        chat_list = self.win.child_window(title='折叠的群聊', control_type='List')
        if chat_list.exists(timeout=0.2):
            self.click_one(title='折叠的群聊', control_type='List', coord_type=3)
            self.send_keys('{HOME}')
        while True:
            box_list = self.win.child_window(title='折叠的群聊', control_type='List')
            if box_list.exists(timeout=0.2):
                if box_list[0].window_text() == first_item:
                    box_button = self.win.child_window(title='折叠的群聊', control_type='Button')
                    if box_button.exists(timeout=0.2):
                        self.click_one(title='折叠的群聊', control_type='Button')
                    self.log.info('当前页第一个群与上一次记录的群一致,折叠群中的数据采集完毕, 退出')
                    return
                first_item = box_list[0].window_text()
                for i in box_list:
                    if [i.window_text()] in self.group_list:
                        pass
                    else:
                        self.log.info(f'将{i.window_text()}添加到群列表中')
                        self.group_list.append([i.window_text()])

            self.log.info('当前页面已经采集完毕, 准备翻到下一页')
            position = self.win.child_window(title="折叠的群聊", control_type="List", found_index=0).rectangle()
            mouse.click(button="left", coords=(int(position.right - 5 * screen_scale_rate),
                                               int(position.bottom - self.bottom * screen_scale_rate)))

    def test(self):
        pass

    def run(self):
        if self.init_chat_page():
            self.go_top()
            try:
                while not self.get_page_info():
                    pass
            except Exception as e:
                self.log.error(f'获取群信息过程中出错{e}')
                return
            self.test()
            self.log.info(f'一共采集到{len(self.group_list)}个群聊')
            if self.group_list:
                self.log.info(f'{self.group_list}')
            return self.group_list
        else:
            return "WeChat not found!"


if __name__ == '__main__':
    # obj = QuitGroupChat()
    # print(obj.quit_group())
    obj2 = FindGroup()
    obj2.go_top()
    obj2.test()

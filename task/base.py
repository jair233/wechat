#!/usr/bin/python
# -*- coding: UTF-8 -*-
from time import sleep

from psutil import process_iter
from pywinauto import mouse, keyboard
from pywinauto.application import Application


from logger.logger import Logger
from constants import screen_scale_rate


class Base:

    def __init__(self):
        self.log = Logger()
        pid = self.__get_pid()
        if not pid:
            self.log.info('未检测到微信运行')
        else:
            # 在这里要设置应用后端，即 uia 或 win32，这里微信要设置为 uia，具体方法可参考文末链接
            self.app = Application(backend='uia')
            self.app.connect(process=pid)
            self.win = self.app[u'微信']
            self.log.info('已建立与微信链接')

    def __get_pid(self):
        pid = process_iter()
        for pid_temp in pid:
            pid_dic = pid_temp .as_dict(attrs=['pid', 'name'])
            if pid_dic['name'] == 'WeChat.exe':
                return pid_dic['pid']

    def click_one(self, title='', control_type='', button='left', found_index=0, coord_type=5, inside=5, times=1):
        """
        :param title: 控件名称
        :param control_type: 控件类型
        :param button: left right middle 对应左、右、中三个键
        :param times: 点击次数
        :param found_index: 选择同名同类型多个控件中的第几个
        :param coord_type: 点击的位置: 1.左上角 2.上中间, 3.右上角 4.左中间 5.正中间
        6.右中间 7.左下角 8.下中间 9.右下角
        :param inside: 向中间靠的像素值
        :return:
        """
        # 在这里控件名称和类型，可以只选择一个，不过定位精准度不高
        inside = int(inside*screen_scale_rate)
        if title == '' and control_type == '':
            raise ValueError('控件名和控件类型不可为空!')
        position = self.win.child_window(title=title, control_type=control_type, found_index=found_index).rectangle()
        left = position.left
        top = position.top
        right = position.right
        bottom = position.bottom
        row_middle = int((right-left)/2)
        col_middle = int((bottom-top)/2)
        if coord_type == 1:
            coord = (left+inside, top+inside)
            self.log.info(f"点击找到的'{title if title else control_type}'元素的左上角位置")
        elif coord_type == 2:
            coord = (left+row_middle, top+inside)
            self.log.info(f"点击找到的'{title if title else control_type}'元素的上中位置")
        elif coord_type == 3:
            coord = (right-inside, top+inside)
            self.log.info(f"点击找到的'{title if title else control_type}'元素的右上角位置")
        elif coord_type == 4:
            coord = (left+inside, top+col_middle)
            self.log.info(f"点击找到的'{title if title else control_type}'元素的左中位置")
        elif coord_type == 6:
            coord = (right-inside, top+col_middle)
            self.log.info(f"点击找到的'{title if title else control_type}'元素的右中位置")
        elif coord_type == 7:
            coord = (left+inside, bottom-inside)
            self.log.info(f"点击找到的'{title if title else control_type}'元素的左下角位置")
        elif coord_type == 8:
            coord = (left+row_middle, bottom-inside)
            self.log.info(f"点击找到的'{title if title else control_type}'元素的下中位置")
        elif coord_type == 9:
            coord = (right-inside, bottom-inside)
            self.log.info(f"点击找到的'{title if title else control_type}'元素的右下角位置")
        else:
            coord = (left+row_middle, top+col_middle)
            self.log.info(f"点击找到的'{title if title else control_type}'元素的正中间位置")
        for i in range(times):
            mouse.click(button=button, coords=coord)
            if times > 1:
                sleep(0.2)
    def send_keys(self, key=''):
        if key:
            keyboard.send_keys(key)
        else:
            raise ValueError('键盘输入的值不可为空')

if __name__ == '__main__':
    obj = Base()

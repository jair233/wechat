import os
import shutil


def del_files(path_file):

    ls = os.listdir(path_file)
    for i in ls:
        f_path = os.path.join(path_file, i)
        print(f_path)
        # 判断是否是一个目录,若是,则递归删除
        if os.path.isdir(f_path):
            shutil.rmtree(f'{f_path }')
        else:
            os.remove(f_path)


if __name__ == "__main__":
    # 删除当前test目录下所有文件
    del_files(r"C:\Users\12157\AppData\Roaming\Tencent\WeChat\XPlugin\Plugins\WMPFRuntime")

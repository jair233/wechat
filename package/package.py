import os
from os import system
import shutil

from constants import PROJECT_PATH


def delete_file(name, version):
    try:
        os.remove(f'{name}{version}.spec')
        print(f'删除了已经存在的"{name}{version}.spec"文件')
    except:
        pass
    try:
        os.remove(f'./outfile/{name}{version}.exe')
        print(f'删除了已经存在的"{name}{version}.exe"文件')
    except:
        pass
    try:
        shutil.rmtree(f'./dist/{name}{version}')
        print(f'删除了已经存在的"{name}{version}"文件夹')
    except:
        pass


def execute_pyinstaller(name, version, ico, main_file):
    bat = f'pyinstaller -D -w -i "{ico}" -n "{name}{version}" {main_file} --add-data "{ico};."'
    system(bat)
    os.rename(f"./dist/{name}{version}/{name}{version}.exe",
              f"./dist/{name}{version}/{name}.exe")


def create_package(name, version, ico, aid, inn_path):
    # 生成安装包程序
    a = ''
    if inn_path:
        a = f'''{inn_path[0]}:
cd "{inn_path}"
'''
    installer = f'ISCC /DMyAppName="{name}" /DMyAppIco="{ico}" /DAppId="{aid}" ' \
                f'/DMyAppVersion="{version}" {PROJECT_PATH}/package/package.iss\n'
    with open('./package.bat', 'w', encoding='utf8') as fp:
        fp.write(a+installer)
    system('./package.bat')


if __name__ == '__main__':
    app_name = "QuitWeChatGroup"   # 软件名称, 也是安装软件时后根目录的名称
    app_version = input('请输入软件的版本')     # 软件版本号
    app_ico = f'{PROJECT_PATH}/ico.ico'     # 软件图标路径
    py_file = f'{PROJECT_PATH}/main.py'     # 打包的主文件

    # 执行前判断一下是否生成相同版本的程序包, 如果有就删除相同的文件
    delete_file(name=app_name, version=app_version)
    # 将Python程序打包成exe执行文件
    execute_pyinstaller(name=app_name, version=app_version, ico=app_ico, main_file=py_file)

    # AppId: 没研究是什么, 大概是Windows识别某一个已经安装好的应用的唯一id(猜测), 自行百度
    app_id = "9DB7D8F0-D497-4AD8-A331-D3E74FD2C0DC"
    inno_path = 'D:/Program Files (x86)/Inno Setup 6'  # Inno setup Compiler的安装目录, 这是我的安装目录
    # 如果需要打包成安装程序, 就执行下面的函数, 但电脑上必须安装了Inno setup Compiler
    # 将程序打包成安装程序, 如果没有在outfile文件夹中生成对应版本的安装程序, 则需要在命令行中手动执行package.bat文件
    create_package(name=app_name, version=app_version, ico=app_ico, aid=app_id, inn_path=inno_path)

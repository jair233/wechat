; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#ifndef MyAppName
# define MyAppName "GoAwayWeChat"
#endif
#ifndef MyAppVersion
# define MyAppVersion "1.0.0"
#endif
#ifndef MyAppPublisher
#define MyAppPublisher "Jair"
#endif
#ifndef MyAppExeName
#define MyAppExeName "GoAwayWeChat.exe"
#endif
#ifndef MyAppIco
#define MyAppIco "../ico.ico"
#endif
#ifndef AppId
#define AppId "9DB7D8F0-D497-4AD8-A331-D3E74FD2C0DC"
#endif

[Setup]
; NOTE: The value of AppId uniquely identifies this application. Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{{#AppId}}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName}{#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName={autopf}\{#MyAppName}
DisableProgramGroupPage=yes
; Remove the following line to run in administrative install mode (install for all users.)
PrivilegesRequired=lowest
OutputDir=.\outfile
OutputBaseFilename={#MyAppName}_{#MyAppVersion}
SetupIconFile={#MyAppIco}
UninstallDisplayIcon={#MyAppIco}
Compression=lzma
SolidCompression=yes
WizardStyle=modern

[Languages]
Name: "chinesesimplified"; MessagesFile: ".\data\ChineseSimplified.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: checkablealone

[Files]
Source: ".\dist\{#MyAppName}{#MyAppVersion}\{#MyAppName}.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: ".\dist\{#MyAppName}{#MyAppVersion}\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent

[UninstallDelete]
Type: files; Name: "{app}\{#MyAppExeName}"
Type: filesandordirs; Name: "{app}\{#MyAppName}"
Type: files; Name: "{app}\cookie.json"


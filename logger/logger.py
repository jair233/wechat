#!/usr/bin/python
# -*- coding: UTF-8 -*-
from os.path import exists
from os import mkdir
import logging
import logging.handlers


from constants import PROJECT_PATH


class Logger(logging.Logger):
    def __init__(self, filename=None):
        super(Logger, self).__init__(filename)
        if exists(f'{PROJECT_PATH}/log_file') is False:
            mkdir(f'{PROJECT_PATH}/log_file')
        log_name = f'{PROJECT_PATH}/log_file/wechat.log'

        # 日志文件名
        if filename is None:
            filename = log_name
        self.filename = filename

        # 创建一个handler，用于写入日志文件 (每天生成1个，保留5天的日志)
        fh = logging.handlers.TimedRotatingFileHandler(self.filename, 'D', 1, 5)
        fh.suffix = "%Y%m%d-%H%M.log"
        fh.setLevel(logging.DEBUG)

        # 再创建一个handler，用于输出到控制台
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)

        fh = logging.FileHandler(self.filename, encoding='utf-8')
        # 定义handler的输出格式
        formatter = logging.Formatter(
            "[%(levelname)s] [%(asctime)s] [%(pathname)s] [%(lineno)d]: %(message)s"
        )
        formatter2 = logging.Formatter("[%(levelname)s] [%(asctime)s] [%(pathname)s] [%(lineno)d]: %(message)s")
        # fh.setFormatter(formatter)
        ch.setFormatter(formatter2)

        # 给logger添加handler
        self.addHandler(fh)
        self.addHandler(ch)


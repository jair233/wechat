#!/usr/bin/python
# -*- coding: UTF-8 -*-
from os.path import exists
from os import mkdir

from constants import PROJECT_PATH, FORDLER_LIST
from logger.logger import Logger

log = Logger()


class Base:
    def __init__(self):
        log.info('检查项目文件')
        for i in FORDLER_LIST:
            if exists(f'{PROJECT_PATH}/{i}') is False:
                log.info(f'创建{i}')
                mkdir(f'{PROJECT_PATH}/{i}')

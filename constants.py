from os.path import abspath, join, dirname
from win32 import win32api, win32gui, win32print
from win32.lib import win32con

"""获取缩放后的分辨率"""
sX = win32api.GetSystemMetrics(0)   #获得屏幕分辨率X轴
sY = win32api.GetSystemMetrics(1)   #获得屏幕分辨率Y轴

"""获取真实的分辨率"""
hDC = win32gui.GetDC(0)
w = win32print.GetDeviceCaps(hDC, win32con.DESKTOPHORZRES)  # 横向分辨率
h = win32print.GetDeviceCaps(hDC, win32con.DESKTOPVERTRES)  # 纵向分辨率

# 缩放比率
screen_scale_rate = round(w / sX, 2)

PROJECT_PATH = abspath(join(dirname(__file__), ".")).replace('\\', '/')  # 日志信息保存地址
MODEL_FILE_PATH = f'{PROJECT_PATH}/model'
MODEL_FILE_MSG = [['群名称'], ['请从这里开始填写群名称(测试群1)'], ['测试群2'], ['测试群3'],
              ['测试群......'], ['......'], ['填写完请另存为一个新文件, 请不要保存为模板文件']]

FORDLER_LIST = ['execute_data', "model"]

APP_NAME = "QuitWeChatGroup_V2.0.0"
if __name__ == '__main__':
    print(PROJECT_PATH)
    print(sX)
    print(sY)

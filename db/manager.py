# create_table_orm.py
from datetime import datetime
from os.path import dirname


from sqlalchemy import Column, String, create_engine, Integer, DateTime, Boolean, text
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base


from constants import PROJECT_PATH
# 创建基类
BASE = declarative_base()


# 定义学生对象
class TaskConfig(BASE):
    # 表的名字:STUDENT
    __tablename__ = 'task_config'
    __describe__ = "任务配置表"
    # 学号
    id = Column(Integer, comment='id', primary_key=True, autoincrement=True)
    name = Column(String(50))
    type = Column(Integer, comment='1.好友 2.群')
    execute_type = Column(Integer, comment='1.删除群 2.')
    status = Column(Boolean, default=False, server_default=text('0'))
    created_time = Column(DateTime, default=datetime.now, comment='创建时间')
    updatted_time = Column(DateTime, default=datetime.now, onupdate=datetime.now, comment='更新时间时间')
    __table_args__ = {
        "mysql_charset": "utf8mb4"
    }


class DbManage:
    __engine = None
    __instance = None

    def __new__(cls, *args, **kwargs):
        if not cls.__instance:
            cls.__instance = object.__new__(cls)
            sqlite_obj = f'sqlite:///{PROJECT_PATH}/db/config.db'
            cls.__engine = create_engine(sqlite_obj, encoding='utf-8', echo=False)
            BASE.metadata.create_all(cls.__engine)
            print(1)

    @staticmethod
    def session():
        SessionClass = sessionmaker(bind=DbManage.__engine)
        session = SessionClass()
        return session
    def __repr__(self):
        return f'{self.__engine}'
if __name__ == '__main__':
    DbManage()
    # .session().query(User).filter(User.id == 1).first()
    print(DbManage())

